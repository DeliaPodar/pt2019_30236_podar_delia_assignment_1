package polinoame;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

public class PolinomView extends JFrame
{
	private static final long serialVersionUID = 1L;

	//public JButton bAdunare=new JButton("Adunare");
	//public JButton bScadere=new JButton("Scadere");
	//public JButton bInmultire=new JButton("Inmultire");
	//public JButton bImpartire=new JButton("Impartire");
	//public JButton bDerivare=new JButton("Derivare");
	//public JButton bIntegrare=new JButton("Integrare");
	//public JButton bRadacini=new JButton("Radacini");
	//public JButton bGrafic=new JButton("Grafic");
	
	public JComboBox<Object> combo=new JComboBox<Object>(new String[]{"Adunare","Scadere","Inmultire","Impartire","Derivare","Integrare","Radacini intregi"});
	
	public JButton bOperatie=new JButton("Next");
	public JButton bGrad=new JButton("Next");
	public JButton bRez=new JButton("Next");
	
	public JLabel lOperatie=new JLabel("Alegeti operatia : ");
	public JLabel lGrad1=new JLabel("Dati gradul polinomului P :");
	public JLabel lGrad2=new JLabel("Dati gradul polinomului G :");
	public JLabel lP=new JLabel("P(X)= ");
	public JLabel lG=new JLabel("Q(X)= ");
	public JLabel lPP=new JLabel("P(X)= ");
	public JLabel lGG=new JLabel("Q(X)= ");
	public JLabel lRez=new JLabel("Rezultat:");
	
	public JTextField tG1=new JTextField(5);
	public JTextField tG2=new JTextField(5);
	
	public JLabel l11=new JLabel("*X^0 +");
	public JLabel l12=new JLabel("*X^1 +");
	public JLabel l13=new JLabel("*X^2 +");
	public JLabel l14=new JLabel("*X^3 +");
	public JLabel l15=new JLabel("*X^4 +");
	public JLabel l16=new JLabel("*X^5 +");
	public JLabel l17=new JLabel("*X^6 +");
	public JLabel l18=new JLabel("*X^7 +");
	public JLabel l19=new JLabel("*X^8 +");
	public JLabel l110=new JLabel("*X^9 +");
	public JLabel l111=new JLabel("*X^10");
	
	public JLabel l21=new JLabel("*X^0 +");
	public JLabel l22=new JLabel("*X^1 +");
	public JLabel l23=new JLabel("*X^2 +");
	public JLabel l24=new JLabel("*X^3 +");
	public JLabel l25=new JLabel("*X^4 +");
	public JLabel l26=new JLabel("*X^5 +");
	public JLabel l27=new JLabel("*X^6 +");
	public JLabel l28=new JLabel("*X^7 +");
	public JLabel l29=new JLabel("*X^8 +");
	public JLabel l210=new JLabel("*X^9 +");
	public JLabel l211=new JLabel("*X^10");
	
	public JTextField t11=new JTextField(5);
	public JTextField t12=new JTextField(5);
	public JTextField t13=new JTextField(5);
	public JTextField t14=new JTextField(5);
	public JTextField t15=new JTextField(5);
	public JTextField t16=new JTextField(5);
	public JTextField t17=new JTextField(5);
	public JTextField t18=new JTextField(5);
	public JTextField t19=new JTextField(5);
	public JTextField t110=new JTextField(5);
	public JTextField t111=new JTextField(5);
	
	public JTextField t21=new JTextField(5);
	public JTextField t22=new JTextField(5);
	public JTextField t23=new JTextField(5);
	public JTextField t24=new JTextField(5);
	public JTextField t25=new JTextField(5);
	public JTextField t26=new JTextField(5);
	public JTextField t27=new JTextField(5);
	public JTextField t28=new JTextField(5);
	public JTextField t29=new JTextField(5);
	public JTextField t210=new JTextField(5);
	public JTextField t211=new JTextField(5);
	
	JPanel panel2=new JPanel();
	JPanel panel21=new JPanel();
	JPanel panel22=new JPanel();
	JPanel panel23=new JPanel();
	JPanel panel3=new JPanel();
	JPanel panel4=new JPanel();
	JPanel panel5=new JPanel();
	JPanel panel6=new JPanel();
	
	
	public PolinomView()
	{
		this.setTitle("Operatii cu polinoame");
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		JPanel panel=new JPanel();
		JPanel panel1=new JPanel();
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel23.setLayout(new BoxLayout(panel23, BoxLayout.Y_AXIS));
		panel6.setLayout(new BoxLayout(panel6, BoxLayout.Y_AXIS));
	
		
		
		Color c3=Color.DARK_GRAY;
		
		panel.setBackground(c3);
		panel1.setBackground(c3);
		panel2.setBackground(c3);
		panel21.setBackground(c3);
		panel22.setBackground(c3);
		panel23.setBackground(c3);
		panel3.setBackground(c3);
		panel4.setBackground(c3);
		panel5.setBackground(c3);
		panel6.setBackground(c3);
		
		
		Color c1=new Color(0, 139, 139);
		
		Color c2=new Color(240, 230, 140);
		bOperatie.setBackground(c1);
		bGrad.setBackground(c1);
		bRez.setBackground(c1);
		
		bOperatie.setForeground(c2);
		bGrad.setForeground(c2);
		bRez.setForeground(c2);
		
		
		
		combo.setBackground(c2);
		combo.setForeground(c1);
		
		lOperatie.setForeground(c2);
		lGrad1.setForeground(c2);
		lGrad2.setForeground(c2);
		lP.setForeground(c2);
		lG.setForeground(c2);
		lPP.setForeground(c2);
		lGG.setForeground(c2);
		lRez.setForeground(c2);
		
		l11.setForeground(c2);
		l12.setForeground(c2);
		l13.setForeground(c2);
		l14.setForeground(c2);
		l15.setForeground(c2);
		l16.setForeground(c2);
		l17.setForeground(c2);
		l18.setForeground(c2);
		l19.setForeground(c2);
		l110.setForeground(c2);
		l111.setForeground(c2);
		
		l21.setForeground(c2);
		l22.setForeground(c2);
		l23.setForeground(c2);
		l24.setForeground(c2);
		l25.setForeground(c2);
		l26.setForeground(c2);
		l27.setForeground(c2);
		l28.setForeground(c2);
		l29.setForeground(c2);
		l210.setForeground(c2);
		l211.setForeground(c2);
		
		t11.setBackground(c1);
		t11.setForeground(c2);
		t12.setBackground(c1);
		t12.setForeground(c2);
		t13.setBackground(c1);
		t13.setForeground(c2);
		t14.setBackground(c1);
		t14.setForeground(c2);
		t15.setBackground(c1);
		t15.setForeground(c2);
		t16.setBackground(c1);
		t16.setForeground(c2);
		t17.setBackground(c1);
		t17.setForeground(c2);
		t18.setBackground(c1);
		t18.setForeground(c2);
		t19.setBackground(c1);
		t19.setForeground(c2);
		t110.setBackground(c1);
		t110.setForeground(c2);
		t111.setBackground(c1);
		t111.setForeground(c2);
		
		t21.setBackground(c1);
		t21.setForeground(c2);
		t22.setBackground(c1);
		t22.setForeground(c2);
		t23.setBackground(c1);
		t23.setForeground(c2);
		t24.setBackground(c1);
		t24.setForeground(c2);
		t25.setBackground(c1);
		t25.setForeground(c2);
		t26.setBackground(c1);
		t26.setForeground(c2);
		t27.setBackground(c1);
		t27.setForeground(c2);
		t28.setBackground(c1);
		t28.setForeground(c2);
		t29.setBackground(c1);
		t29.setForeground(c2);
		t210.setBackground(c1);
		t210.setForeground(c2);
		t211.setBackground(c1);
		t211.setForeground(c2);
		
		tG1.setBackground(c2);
		tG1.setForeground(c1);
		tG2.setBackground(c2);
		tG2.setForeground(c1);
		
		
		
		panel1.add(lOperatie);
		panel1.add(combo);
		panel1.add(bOperatie);
		
		panel21.add(lGrad1);
		panel21.add(tG1);
		panel22.add(lGrad2);
		panel22.add(tG2);
		panel23.add(panel21);
		panel23.add(panel22);
		panel2.add(panel23);
		panel2.add(bGrad);
		
		panel3.add(lP);
		panel3.add(t11);
		panel3.add(l11);
		panel3.add(t12);
		panel3.add(l12);
		panel3.add(t13);
		panel3.add(l13);
		panel3.add(t14);
		panel3.add(l14);
		panel3.add(t15);
		panel3.add(l15);
		panel3.add(t16);
		panel3.add(l16);
		panel3.add(t17);
		panel3.add(l17);
		panel3.add(t18);
		panel3.add(l18);
		panel3.add(t19);
		panel3.add(l19);
		panel3.add(t110);
		panel3.add(l110);
		panel3.add(t111);
		panel3.add(l111);
		
		panel4.add(lG);
		panel4.add(t21);
		panel4.add(l21);
		panel4.add(t22);
		panel4.add(l22);
		panel4.add(t23);
		panel4.add(l23);
		panel4.add(t24);
		panel4.add(l24);
		panel4.add(t25);
		panel4.add(l25);
		panel4.add(t26);
		panel4.add(l26);
		panel4.add(t27);
		panel4.add(l27);
		panel4.add(t28);
		panel4.add(l28);
		panel4.add(t29);
		panel4.add(l29);
		panel4.add(t210);
		panel4.add(l210);
		panel4.add(t211);
		panel4.add(l211);
		
		
		
		panel5.add(bRez);
		
		panel6.add(lPP);
		panel6.add(lGG);
		panel6.add(lRez);
		
		panel.add(panel1);
		panel.add(panel2);
		panel.add(panel3);
		panel.add(panel4);
		panel.add(panel5);
		panel.add(panel6);
		
		panel2.setVisible(false);
		panel3.setVisible(false);
		panel4.setVisible(false);
		panel5.setVisible(false);
		panel6.setVisible(false);
		
		this.setContentPane(panel);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	
	String getUserInput(JTextField tf) 
	 {
	        return tf.getText();
	 }
	
	 void addBOperatieListener(ActionListener mal) 
	 {
	        bOperatie.addActionListener(mal);
	 }
	 
	 void addBGradListener(ActionListener mal) 
	 {
	        bGrad.addActionListener(mal);
	 }
	 
	 void addBRezListener(ActionListener mal) 
	 {
	        bRez.addActionListener(mal);
	 }
}
