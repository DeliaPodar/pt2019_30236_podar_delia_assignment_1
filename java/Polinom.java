package polinoame;



public class Polinom

{
	public static int GRAD_MAX=10;
	
	public float[] coef;
	public int grad;
	
	public Polinom ()
	{
		this.grad=0;
		this.coef=new float[GRAD_MAX+1];
		
	}
	
	public Polinom(int grad)
	{
		this.grad=grad;
		this.coef=new float[grad+1];
	}
	public Polinom(int grad, float[] coef)
	{
		this.grad=grad;
		this.coef=coef;
	}

	public void setOneCoef(int index,float coeficient)
	{
		this.coef[index]=coeficient;
	}
	
	public int getGradMin(Polinom p)
	{
		if(this.grad>p.grad)
			return p.grad;
		else return this.grad;
	}
	
	public int getGradMax(Polinom p)
	{
		if(this.grad<p.grad)
			return p.grad;
		else return this.grad;
	}
	
	public Polinom getPolMax(Polinom p)
	{
		if(this.grad>p.grad)
			return this;
		else return p;
	}
	
	public Polinom adunare(Polinom p)
	{
		int max=this.getGradMax(p);
		int min=this.getGradMin(p);
		Polinom suma=new Polinom(max);
		
		for(int i=0;i<=min;i++)
		{
			suma.setOneCoef(i,this.coef[i]+p.coef[i]);
		}
		if(this.grad!=p.grad)
		{
			Polinom g=this.getPolMax(p);
			for(int i=min+1;i<=max;i++)
			{
				suma.setOneCoef(i,g.coef[i]);
			}
		}
		return suma;	
	}
	
	public Polinom scadere(Polinom p)
	{
		int max=this.getGradMax(p);
		int min=this.getGradMin(p);
		Polinom dif=new Polinom(max);
		
		for(int i=0;i<=min;i++)
		{
			dif.setOneCoef(i,this.coef[i]-p.coef[i]);
		}
		if(this.grad<p.grad)
		{
			for(int i=min+1;i<=max;i++)
			{
				dif.setOneCoef(i,-p.coef[i]);
			}
		}
		else
		if(this.grad>p.grad)
		{
			for(int i=min+1;i<=max;i++){
				dif.setOneCoef(i,this.coef[i]);
			}
		}
		else
			while(dif.coef[min]==0 && dif.grad>0)
			{
				dif.grad--;
				min--;
			}
		return dif;	
	}
	
	public  Polinom inmultire(Polinom p)
	{
		Polinom rez=new Polinom(this.grad+p.grad);
		Polinom aux;
		for(int i=0;i<=this.grad;i++)
			for(int j=0;j<=p.grad;j++)
			{
				aux=new Polinom(i+j);
				aux.setOneCoef(i+j, this.coef[i]*p.coef[j]);
				rez=rez.adunare(aux);
			}
		return rez;
	}
	
	
	public PolinomRest impartire(Polinom p)
	{
		int i=this.grad-p.grad;
		int j=this.grad;
		int k=p.grad;
		PolinomRest rezultat=new PolinomRest();
		Polinom cat=new Polinom(i);
		Polinom catAux;
		Polinom rest=this;
		Polinom restAux;
		
		while(rest.grad>=p.grad)
		{
			catAux=new Polinom(i);
			catAux.setOneCoef(i, rest.coef[j]/p.coef[k]);
			restAux=p.inmultire(catAux);
			rest=rest.scadere(restAux);
			cat=cat.adunare(catAux);
			i=rest.grad-p.grad;
			j=rest.grad;
		}
		rezultat.setCat(cat);
		rezultat.setRest(rest);
		return rezultat;
	}
	
	
	public Polinom derivare()
	{
		Polinom rez=new Polinom(this.grad-1);
		for( int i=1;i<=this.grad;i++)
		{
			rez.setOneCoef(i-1,i*this.coef[i]);
		}
		return rez;
	}
	
	
	public Polinom integrare(float constanta)
	{
		Polinom rez=new Polinom(this.grad+1);
		rez.setOneCoef(0,constanta);
		for( int i=0;i<=this.grad;i++)
		{
			rez.setOneCoef(i+1,this.coef[i]/(i+1));
		}
		return rez;
	}
	
	
	public float valoarePolinom(float x)
	{
		float rez=0;
		for(int i=0;i<=this.grad;i++)
			rez+=Math.pow(x,i)*this.coef[i];
		return rez;
	}
	
	public Polinom radaciniIntregi()
	{
		Polinom p=new Polinom(this.grad);
		int contor=-1;
		float x=Math.abs(this.coef[0]);
		Polinom p1=this;
		Polinom p2=this;
		PolinomRest polRest=new PolinomRest();
		boolean ok=false;
		
		if(x==0)
		{
			ok=true;
			int k=0;
			while(this.coef[k]==0 && k<=this.grad)
				k++;
			Polinom pol=new Polinom(k);
			pol.setOneCoef(k,1);
			polRest=this.impartire(pol);
			x=Math.abs(polRest.cat.coef[0]);
			while(k>0)
			{
				contor++;
				p.setOneCoef(contor,0);
				k--;
			}
		}
		
		for(int i=1;i<=x;i++)
		{
				if(x%i==0){
					if(ok==true)
					{
						p1=polRest.cat;
						p2=polRest.cat;
					}
					else
					{
						 p1=this;
						 p2=this;
					}
					while(p1.valoarePolinom(i)==0)
					{
						contor++;
						p.setOneCoef(contor,i);
						p1=p1.derivare();
					}
					while(p2.valoarePolinom(-i)==0)
					{
						contor++;
						p.setOneCoef(contor,-i);
						p2=p2.derivare();
					}
				}
			}
		p.grad=contor;
		return p;
	}
	
	public String afisare()
	{
		String rezultat="";
		if(coef[0]<0)
			rezultat=rezultat+" - ";
		for(int i=0;i<=grad;i++)
		{
			if(coef[i]!=0)
			{
				int x=(int)coef[i];
				if(i==0)
					{if((float)x==coef[i])
						rezultat=rezultat+Math.abs(x);
					else
						rezultat=rezultat+Math.abs(coef[i]);
					}
				else
				{
				if(coef[i]==1||coef[i]==-1)
					rezultat=rezultat+"X^"+i;
				else
				{
					if((float)x==coef[i])
						rezultat=rezultat+Math.abs(x)+"*X^"+i;
					else
						rezultat=rezultat+Math.abs(coef[i])+"*X^"+i;
				
				}
				}
				if(i!=grad)
					{if(coef[i+1]>0)
						rezultat=rezultat+" + ";
					if(coef[i+1]<0)
						rezultat=rezultat+" - ";
					}
						
				
			}
			else
			{
				if(grad==0)
					rezultat=rezultat+"0";
				else
				{
					if(i!=grad)
					{
						if(coef[i+1]<0)
							rezultat=rezultat+" - ";
					if(coef[i+1]>0)
						rezultat=rezultat+" + ";
					}
				}
			}
			
		}
		return rezultat;
	}
	

	
}

