package polinoame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class PolinomController 
{
	public PolinomView view;
	public Polinom p,q;
	
	public PolinomController(PolinomView view,Polinom p,Polinom q)
	{
		this.view=view;
		this.p=p;
		this.q=q;
		view.addBOperatieListener(new BOperatieListener());
		view.addBGradListener(new BGradListener());
		view.addBRezListener(new BRezListener());
		
	}
	
	class BOperatieListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			String comboBox=(String)view.combo.getSelectedItem();
			view.panel2.setVisible(true);
			view.panel3.setVisible(false);
			view.panel4.setVisible(false);
			view.panel5.setVisible(false);
			view.panel6.setVisible(false);
			view.t11.setText("");
			view.t12.setText("");
			view.t13.setText("");
			view.t14.setText("");
			view.t15.setText("");
			view.t16.setText("");
			view.t17.setText("");
			view.t18.setText("");
			view.t19.setText("");
			view.t110.setText("");
			view.t111.setText("");
			
			view.t21.setText("");
			view.t22.setText("");
			view.t23.setText("");
			view.t24.setText("");
			view.t25.setText("");
			view.t26.setText("");
			view.t27.setText("");
			view.t28.setText("");
			view.t29.setText("");
			view.t210.setText("");
			view.t211.setText("");
			
			view.tG1.setText("");
			view.tG2.setText("");
			switch(comboBox)
			{
				case "Adunare":
				case "Scadere":
				case "Inmultire":
				case "Impartire":
					{
						view.panel22.setVisible(true);
						break;
					}
				case "Derivare":
				case "Integrare":
				case "Radacini intregi":
				case "Grafic":
					{	
						view.panel22.setVisible(false);
						break;
					}
			}
			
			
		}
		
	}
	
	class BGradListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			String comboBox=(String)view.combo.getSelectedItem();
			String grad1=view.getUserInput(view.tG1);
			String grad2=view.getUserInput(view.tG2);
			
			try
			{
				
				int g1=Integer.parseInt(grad1);
				int g2=0;
				switch(comboBox)
				{
					case "Adunare":
					case "Scadere":
					case "Inmultire":
					case "Impartire":
						{
							g2=Integer.parseInt(grad2);
							break;
						}
				}
				
				if(g1<0||g1>10||g2<0||g2>10)
					throw new Exception();
				
				view.panel2.setVisible(true);
				view.panel3.setVisible(true);
				view.panel5.setVisible(true);
				view.panel6.setVisible(false);
				
				view.t11.setVisible(true);
				view.t12.setVisible(true);
				view.t13.setVisible(true);
				view.t14.setVisible(true);
				view.t15.setVisible(true);
				view.t16.setVisible(true);
				view.t17.setVisible(true);
				view.t18.setVisible(true);
				view.t19.setVisible(true);
				view.t110.setVisible(true);
				view.t111.setVisible(true);
				
				view.t21.setVisible(true);
				view.t22.setVisible(true);
				view.t23.setVisible(true);
				view.t24.setVisible(true);
				view.t25.setVisible(true);
				view.t26.setVisible(true);
				view.t27.setVisible(true);
				view.t28.setVisible(true);
				view.t29.setVisible(true);
				view.t210.setVisible(true);
				view.t211.setVisible(true);
				
				view.l11.setText("*X^0 +");
				view.l12.setText("*X^1 +");
				view.l13.setText("*X^2 +");
				view.l14.setText("*X^3 +");
				view.l15.setText("*X^4 +");
				view.l16.setText("*X^5 +");
				view.l17.setText("*X^6 +");
				view.l18.setText("*X^7 +");
				view.l19.setText("*X^8 +");
				view.l110.setText("*X^9 +");
				view.l111.setText("*X^10 +");
				
				view.l21.setText("*X^0 +");
				view.l22.setText("*X^1 +");
				view.l23.setText("*X^2 +");
				view.l24.setText("*X^3 +");
				view.l25.setText("*X^4 +");
				view.l26.setText("*X^5 +");
				view.l27.setText("*X^6 +");
				view.l28.setText("*X^7 +");
				view.l29.setText("*X^8 +");
				view.l210.setText("*X^9 +");
				view.l211.setText("*X^10");
				
				switch (g1)
				{
					case 0: view.t12.setVisible(false);
							view.t13.setVisible(false);
							view.t14.setVisible(false);
							view.t15.setVisible(false);
							view.t16.setVisible(false);
							view.t17.setVisible(false);
							view.t18.setVisible(false);
							view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false); 
							
							view.l12.setText("");
							view.l13.setText("");
							view.l14.setText("");
							view.l15.setText("");
							view.l16.setText("");
							view.l17.setText("");
							view.l18.setText("");
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l11.setText("X^0");
							break;
					case 1: view.t13.setVisible(false);
							view.t14.setVisible(false);
							view.t15.setVisible(false);
							view.t16.setVisible(false);
							view.t17.setVisible(false);
							view.t18.setVisible(false);
							view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false); 
							
							view.l13.setText("");
							view.l14.setText("");
							view.l15.setText("");
							view.l16.setText("");
							view.l17.setText("");
							view.l18.setText("");
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l12.setText("X^1");
							break;
					case 2: view.t14.setVisible(false);
							view.t15.setVisible(false);
							view.t16.setVisible(false);
							view.t17.setVisible(false);
							view.t18.setVisible(false);
							view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false); 
							
							view.l14.setText("");
							view.l15.setText("");
							view.l16.setText("");
							view.l17.setText("");
							view.l18.setText("");
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l13.setText("X^2");
							break;
					case 3: view.t15.setVisible(false);
							view.t16.setVisible(false);
							view.t17.setVisible(false);
							view.t18.setVisible(false);
							view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false); 
							
							view.l15.setText("");
							view.l16.setText("");
							view.l17.setText("");
							view.l18.setText("");
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l14.setText("X^3");
							break;
					case 4: view.t16.setVisible(false);
							view.t17.setVisible(false);
							view.t18.setVisible(false);
							view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false); 
							
							view.l16.setText("");
							view.l17.setText("");
							view.l18.setText("");
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l15.setText("X^4");
							break;
					case 5: view.t17.setVisible(false);
							view.t18.setVisible(false);
							view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false); 
							
							view.l17.setText("");
							view.l18.setText("");
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l16.setText("X^5");
							break;
					case 6: view.t18.setVisible(false);
							view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false);
							
							view.l18.setText("");
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l17.setText("X^6");
							break;
					case 7: view.t19.setVisible(false);
							view.t110.setVisible(false);
							view.t111.setVisible(false); 
							
							view.l19.setText("");
							view.l110.setText("");
							view.l111.setText("");
							
							view.l18.setText("X^7");
							break;
					case 8: view.t110.setVisible(false);
							view.t111.setVisible(false);
							
							view.l110.setText("");
							view.l111.setText("");
							
							view.l19.setText("X^8");
							break;
					case 9: view.t111.setVisible(false);
					
							view.l111.setText("");
					
							view.l110.setText("X^9");
							break;
					default: break;
				}
				
				
				switch (g2)
				{
					case 0: view.t22.setVisible(false);
							view.t23.setVisible(false);
							view.t24.setVisible(false);
							view.t25.setVisible(false);
							view.t26.setVisible(false);
							view.t27.setVisible(false);
							view.t28.setVisible(false);
							view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l22.setText("");
							view.l23.setText("");
							view.l24.setText("");
							view.l25.setText("");
							view.l26.setText("");
							view.l27.setText("");
							view.l28.setText("");
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l21.setText("X^0");
							break;
					case 1: view.t23.setVisible(false);
							view.t24.setVisible(false);
							view.t25.setVisible(false);
							view.t26.setVisible(false);
							view.t27.setVisible(false);
							view.t28.setVisible(false);
							view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l23.setText("");
							view.l24.setText("");
							view.l25.setText("");
							view.l26.setText("");
							view.l27.setText("");
							view.l28.setText("");
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l22.setText("X^1");
							break;
					case 2: view.t24.setVisible(false);
							view.t25.setVisible(false);
							view.t26.setVisible(false);
							view.t27.setVisible(false);
							view.t28.setVisible(false);
							view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l24.setText("");
							view.l25.setText("");
							view.l26.setText("");
							view.l27.setText("");
							view.l28.setText("");
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l23.setText("X^2");
							break;
					case 3: view.t25.setVisible(false);
							view.t26.setVisible(false);
							view.t27.setVisible(false);
							view.t28.setVisible(false);
							view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l25.setText("");
							view.l26.setText("");
							view.l27.setText("");
							view.l28.setText("");
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l24.setText("X^3");
							break;
					case 4: view.t26.setVisible(false);
							view.t27.setVisible(false);
							view.t28.setVisible(false);
							view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l26.setText("");
							view.l27.setText("");
							view.l28.setText("");
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l25.setText("X^4");
							break;
					case 5: view.t27.setVisible(false);
							view.t28.setVisible(false);
							view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l27.setText("");
							view.l28.setText("");
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l26.setText("X^5");
							break;
					case 6: view.t28.setVisible(false);
							view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false);
							
							view.l28.setText("");
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l27.setText("X^6");
							break;
					case 7: view.t29.setVisible(false);
							view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l29.setText("");
							view.l210.setText("");
							view.l211.setText("");
							
							view.l28.setText("X^7");
							break;
					case 8: view.t210.setVisible(false);
							view.t211.setVisible(false); 
							
							view.l210.setText("");
							view.l211.setText("");
							
							view.l29.setText("X^8");
							break;
					case 9: view.t211.setVisible(false); 
					
							view.l211.setText("");
					
							view.l210.setText("X^9");
							break;
					default: break;
				}
				
				switch(comboBox)
				{
					case "Adunare":
					case "Scadere":
					case "Inmultire":
					case "Impartire":
						{
							view.panel4.setVisible(true);
							break;
						}
					case "Derivare":
					case "Integrare":
					case "Radacini intregi":
						{	
							view.panel4.setVisible(false);
							break;
						}
				}
				
			}
			
			catch (NumberFormatException nfex) 
			{
				JOptionPane.showMessageDialog(null,"Gradul trebuie sa fie un intreg!", null, JOptionPane.ERROR_MESSAGE);
		
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(null,"Gradul trebuie sa fie un intreg intre 0 si 10!", null, JOptionPane.ERROR_MESSAGE);
			}
			
			
			
		}
		
	}
	
	class BRezListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			String comboBox=(String)view.combo.getSelectedItem();
			String grad1=view.getUserInput(view.tG1);
			String grad2=view.getUserInput(view.tG2);
			Polinom pol=new Polinom();
			Polinom polinom=new Polinom();
			PolinomRest polRest;
			String rezultat="";
			String s1="",s2="";
			try
			{
				view.panel5.setVisible(true);
				view.panel6.setVisible(true);
				view.lGG.setVisible(true);
				int g1=Integer.parseInt(grad1);
				int g2=0;
				int a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11;
				int b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11;
				
				switch(g1)
				{
					case 0: a1=Integer.parseInt(view.getUserInput(view.t11));
							p.grad=0;
							p.setOneCoef(0,a1);
							break;
					case 1: a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							p.grad=1;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							break;
					case 2: a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							p.grad=2;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							break;
					case 3:	a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							p.grad=3;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							break;
					case 4:	a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							a5=Integer.parseInt(view.getUserInput(view.t15));
							p.grad=4;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							p.setOneCoef(4,a5);
							break;
					case 5:	a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							a5=Integer.parseInt(view.getUserInput(view.t15));
							a6=Integer.parseInt(view.getUserInput(view.t16));
							p.grad=5;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							p.setOneCoef(4,a5);
							p.setOneCoef(5,a6);
							break;
					case 6: a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							a5=Integer.parseInt(view.getUserInput(view.t15));
							a6=Integer.parseInt(view.getUserInput(view.t16));
							a7=Integer.parseInt(view.getUserInput(view.t17));
							p.grad=6;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							p.setOneCoef(4,a5);
							p.setOneCoef(5,a6);
							p.setOneCoef(6,a7);
							break;
					case 7: a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							a5=Integer.parseInt(view.getUserInput(view.t15));
							a6=Integer.parseInt(view.getUserInput(view.t16));
							a7=Integer.parseInt(view.getUserInput(view.t17));
							a8=Integer.parseInt(view.getUserInput(view.t18));
							p.grad=7;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							p.setOneCoef(4,a5);
							p.setOneCoef(5,a6);
							p.setOneCoef(6,a7);
							p.setOneCoef(7,a8);
							break;
					case 8: a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							a5=Integer.parseInt(view.getUserInput(view.t15));
							a6=Integer.parseInt(view.getUserInput(view.t16));
							a7=Integer.parseInt(view.getUserInput(view.t17));
							a8=Integer.parseInt(view.getUserInput(view.t18));
							a9=Integer.parseInt(view.getUserInput(view.t19));
							p.grad=8;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							p.setOneCoef(4,a5);
							p.setOneCoef(5,a6);
							p.setOneCoef(6,a7);
							p.setOneCoef(7,a8);
							p.setOneCoef(8,a9);
							break;
					case 9: a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							a5=Integer.parseInt(view.getUserInput(view.t15));
							a6=Integer.parseInt(view.getUserInput(view.t16));
							a7=Integer.parseInt(view.getUserInput(view.t17));
							a8=Integer.parseInt(view.getUserInput(view.t18));
							a9=Integer.parseInt(view.getUserInput(view.t19));
							a10=Integer.parseInt(view.getUserInput(view.t110));
							p.grad=9;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							p.setOneCoef(4,a5);
							p.setOneCoef(5,a6);
							p.setOneCoef(6,a7);
							p.setOneCoef(7,a8);
							p.setOneCoef(8,a9);
							p.setOneCoef(9,a10);
							break;
					case 10: a1=Integer.parseInt(view.getUserInput(view.t11));
							a2=Integer.parseInt(view.getUserInput(view.t12));
							a3=Integer.parseInt(view.getUserInput(view.t13));
							a4=Integer.parseInt(view.getUserInput(view.t14));
							a5=Integer.parseInt(view.getUserInput(view.t15));
							a6=Integer.parseInt(view.getUserInput(view.t16));
							a7=Integer.parseInt(view.getUserInput(view.t17));
							a8=Integer.parseInt(view.getUserInput(view.t18));
							a9=Integer.parseInt(view.getUserInput(view.t19));
							a10=Integer.parseInt(view.getUserInput(view.t110));
							a11=Integer.parseInt(view.getUserInput(view.t111));
							p.grad=10;
							p.setOneCoef(0,a1);
							p.setOneCoef(1,a2);
							p.setOneCoef(2,a3);
							p.setOneCoef(3,a4);
							p.setOneCoef(4,a5);
							p.setOneCoef(5,a6);
							p.setOneCoef(6,a7);
							p.setOneCoef(7,a8);
							p.setOneCoef(8,a9);
							p.setOneCoef(9,a10);
							p.setOneCoef(10,a11);
							break;
				}
				switch(comboBox)
				{
					case "Adunare":
					case "Scadere":
					case "Inmultire":
					case "Impartire":
						{
							g2=Integer.parseInt(grad2);
							switch(g2)
							{
								case 0: b1=Integer.parseInt(view.getUserInput(view.t21));
										q.grad=0;
										q.setOneCoef(0,b1);
										break;
								case 1: b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										q.grad=1;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										break;
								case 2: b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										q.grad=2;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										break;
								case 3:	b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										q.grad=3;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										break;
								case 4:	b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										b5=Integer.parseInt(view.getUserInput(view.t25));
										q.grad=4;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										q.setOneCoef(4,b5);
										break;
								case 5:	b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										b5=Integer.parseInt(view.getUserInput(view.t25));
										b6=Integer.parseInt(view.getUserInput(view.t26));
										q.grad=5;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										q.setOneCoef(4,b5);
										q.setOneCoef(5,b6);
										break;
								case 6: b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										b5=Integer.parseInt(view.getUserInput(view.t25));
										b6=Integer.parseInt(view.getUserInput(view.t26));
										b7=Integer.parseInt(view.getUserInput(view.t27));
										q.grad=6;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										q.setOneCoef(4,b5);
										q.setOneCoef(5,b6);
										q.setOneCoef(6,b7);
										break;
								case 7: b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										b5=Integer.parseInt(view.getUserInput(view.t25));
										b6=Integer.parseInt(view.getUserInput(view.t26));
										b7=Integer.parseInt(view.getUserInput(view.t27));
										b8=Integer.parseInt(view.getUserInput(view.t28));
										q.grad=7;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										q.setOneCoef(4,b5);
										q.setOneCoef(5,b6);
										q.setOneCoef(6,b7);
										q.setOneCoef(7,b8);
										break;
								case 8: b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										b5=Integer.parseInt(view.getUserInput(view.t25));
										b6=Integer.parseInt(view.getUserInput(view.t26));
										b7=Integer.parseInt(view.getUserInput(view.t27));
										b8=Integer.parseInt(view.getUserInput(view.t28));
										b9=Integer.parseInt(view.getUserInput(view.t29));
										q.grad=8;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										q.setOneCoef(4,b5);
										q.setOneCoef(5,b6);
										q.setOneCoef(6,b7);
										q.setOneCoef(7,b8);
										q.setOneCoef(8,b9);
										break;
								case 9: b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										b5=Integer.parseInt(view.getUserInput(view.t25));
										b6=Integer.parseInt(view.getUserInput(view.t26));
										b7=Integer.parseInt(view.getUserInput(view.t27));
										b8=Integer.parseInt(view.getUserInput(view.t28));
										b9=Integer.parseInt(view.getUserInput(view.t29));
										b10=Integer.parseInt(view.getUserInput(view.t210));
										q.grad=9;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										q.setOneCoef(4,b5);
										q.setOneCoef(5,b6);
										q.setOneCoef(6,b7);
										q.setOneCoef(7,b8);
										q.setOneCoef(8,b9);
										q.setOneCoef(9,b10);
										break;
								case 10: b1=Integer.parseInt(view.getUserInput(view.t21));
										b2=Integer.parseInt(view.getUserInput(view.t22));
										b3=Integer.parseInt(view.getUserInput(view.t23));
										b4=Integer.parseInt(view.getUserInput(view.t24));
										b5=Integer.parseInt(view.getUserInput(view.t25));
										b6=Integer.parseInt(view.getUserInput(view.t26));
										b7=Integer.parseInt(view.getUserInput(view.t27));
										b8=Integer.parseInt(view.getUserInput(view.t28));
										b9=Integer.parseInt(view.getUserInput(view.t29));
										b10=Integer.parseInt(view.getUserInput(view.t210));
										b11=Integer.parseInt(view.getUserInput(view.t211));
										q.grad=10;
										q.setOneCoef(0,b1);
										q.setOneCoef(1,b2);
										q.setOneCoef(2,b3);
										q.setOneCoef(3,b4);
										q.setOneCoef(4,b5);
										q.setOneCoef(5,b6);
										q.setOneCoef(6,b7);
										q.setOneCoef(7,b8);
										q.setOneCoef(8,b9);
										q.setOneCoef(9,b10);
										q.setOneCoef(10,b11);
										break;
							}
							break;
						}
					default : view.lGG.setVisible(false);
						break;
						
				}
				
				switch(comboBox)
				{
					case "Adunare":  pol=p.adunare(q);
									rezultat=rezultat+"P(X) + Q(X) = ";
									break;
					case "Scadere": pol=p.scadere(q);
									rezultat=rezultat+"P(X) - Q(X) = ";
									break;
					case "Inmultire":pol=p.inmultire(q);
									rezultat=rezultat+"P(X) * Q(X) = ";
									break;
					case "Impartire":polRest=p.impartire(q);
									pol=polRest.cat;
									polinom=polRest.rest;
									rezultat=rezultat+"P(X) / Q(X) = ";
									break;
					case "Derivare":pol=p.derivare();
									rezultat=rezultat+"P'(X) = ";
									break;
					case "Integrare":int c=0;
									//JOptionPane.showInputDialog("Dati constanta C :", c);
									pol=p.integrare(c);
									rezultat=rezultat+" Integraga din P(X) = ";
									break;
					case "Radacini intregi": pol=p.radaciniIntregi();
											 break;
				}
				
				s1="P(X) = "+p.afisare();
				s2="Q(X) = "+q.afisare();
				if(comboBox =="Radacini intregi")
				{
					if(pol.grad==-1)
						rezultat=rezultat+"Polinomul P nu are radacini intregi.";
					else
					{
						rezultat=rezultat+"Radacinile intregi ale polinomului P sunt : ";
						for(int i=0;i<=pol.grad;i++)
						{
							int x=(int)pol.coef[i];
							if((float)x==pol.coef[i])
								rezultat=rezultat+"x"+(i+1)+" = "+x+"    ";
							else
								rezultat=rezultat+"x"+(i+1)+" = "+pol.coef[i]+"    ";
						}
					}
				}
				else
				{
					rezultat=rezultat+pol.afisare();
					if(comboBox=="Impartire")
						rezultat=rezultat+"   Rest  "+polinom.afisare();
				}
				
				view.lRez.setText(rezultat);
				view.lPP.setText(s1);
				view.lGG.setText(s2);
			}
			catch (NumberFormatException nfex) 
			{
				JOptionPane.showMessageDialog(null,"Coeficientii trebuie sa fie numere intregi!", null, JOptionPane.ERROR_MESSAGE);
		
			}
		}
	
		
	}
	

}
